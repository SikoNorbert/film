package film;
import java.util.ArrayList;
import java.util.Scanner;
public class Film {
    private String titleOfFilm;
    private ArrayList<String> actors = new ArrayList();
    private String director;
    private int numberOfActors;
    private String yearProductionFilm;


    public String getTitleOfFilm() {
        return this.titleOfFilm;
    }

    public ArrayList<String> getActors() {
        return this.actors;
    }

    public void setActors(ArrayList<String> actors) {
        this.actors = actors;
    }

    public void setTitleOfFilm(String titleOfFilm) {
        this.titleOfFilm = titleOfFilm;
    }

    public String getDirector() {
        return this.director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getNumberOfActors() {
        return this.numberOfActors;
    }

    public void setNumberOfActors(int numberOfActors) {
        this.numberOfActors = numberOfActors;
    }

    public String getYearProductionFilm() {
        return this.yearProductionFilm;
    }

    public void setYearProductionFilm(String yearProductionFilm) {
        this.yearProductionFilm = yearProductionFilm;
    }

    public void detailsFilm() {
        Scanner name = new Scanner(System.in);
        System.out.println("Number of actors: \n");
        this.numberOfActors = name.nextInt();
        System.out.println(this.titleOfFilm + this.actors + this.director + this.yearProductionFilm + this.numberOfActors);
    }

    public void defineActors() {
        for(int i = 0; i < this.numberOfActors; ++i) {
            Scanner s = new Scanner(System.in);
            String actor = s.nextLine();
            this.actors.add(actor);

            System.out.println(this.actors.get(i));
            System.out.println("Actors are " + actors.get(i));
        }

    }

    public void showActors() {
        for(int i = 0; i < this.actors.toArray().length; ++i) {
            System.out.println("Show actors: " + actors.get(i));
        }

    }
}



